Comment accéder au site :

1. composer install
2. vendor/bin/phpunit tests/
3. php -S 127.0.0.1:8000
4. Aller sur [127.0.0.1:8000](http://127.0.0.1:8000/) 



Libraires externes nécessaires : 

mikecao/flight
symfony/process
guzzlehttp/guzzle
phpunit/phpunit
twig/twig
rmccue/requests
michelf/php-markdown